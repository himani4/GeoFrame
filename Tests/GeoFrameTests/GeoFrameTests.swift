import XCTest
@testable import GeoFrame

final class GeoFrameTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(GeoFrame().text, "Hello, World!")
    }
}
