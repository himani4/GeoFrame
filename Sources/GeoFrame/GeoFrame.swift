import GeoFrame
import FBSDKLoginKit
import FBSDKCoreKit

public struct GeoFrame {
    public private(set) var text = "Hello, World!"

    public init() {
        
    }
    
    public func login() -> GeoFrame.LoginVC {

        return Utility.performLogin()
    }
    
    public func logout() {

            return Utility.Logout()
        }
}
