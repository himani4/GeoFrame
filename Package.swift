// swift-tools-version: 5.5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GeoFrame",
    platforms: [.iOS(.v13)],

    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GeoFrame",
            targets: ["GeoFrame"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        
           .package(url: "https://github.com/facebook/facebook-ios-sdk", from: "9.3.0"),
        
           
            .package(url: "https://github.com/tristanhimmelman/ObjectMapper.git", .upToNextMajor(from: "4.1.0")),
           
    ],
    targets: [
                .binaryTarget(name: "GeoFrame", path: "./Sources/GeoFrame.xcframework")
            ]
)
